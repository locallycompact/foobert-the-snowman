let
  f =
    build-or-shell:
    { chan ? "5272327b81ed355bbed5659b8d303cf2979b6953"
    , compiler ? "ghc865"
    , withHoogle ? false
    , doHoogle ? false
    , doHaddock ? false
    , enableLibraryProfiling ? false
    , enableExecutableProfiling ? false
    , strictDeps ? false
    , isJS ? false
    , system ? builtins.currentSystem
    , optimize ? true
    , shpadoinkle-path ? null
    }:
    let

      # It's a shpadoinkle day
      shpadoinkle = if shpadoinkle-path != null then shpadoinkle-path else
      builtins.fetchGit {
        url = https://gitlab.com/platonic/shpadoinkle.git;
        ref = "master";
        rev = "34cfee8702c8c104a211686e9c6c078315b65c0a";
      };

      hackage = builtins.fetchurl {
        url = "https://github.com/commercialhaskell/all-cabal-hashes/archive/80e43c34eaf48fe943dfefd91b618ec3a5b678ca.tar.gz";
        sha256 = "1g52w3vny11qd6kdkdgnrq5wmhfq9mgywhxaks6wz8zygri8mcqg";
      };

      text-manipulate = builtins.fetchGit {
        url = "https://github.com/locallycompact/text-manipulate";
        ref = "main";
        rev = "e1ed472da3c3a076deadf3aecc6b3511ea7561e0";
      };

      polysemy = builtins.fetchGit {
        url = "https://github.com/locallycompact/polysemy";
        ref = "master";
        rev = "5942a398ef02bbbe94bedd68f8628390ed223107";
      };
      dhall = builtins.fetchGit {
        url = "https://github.com/locallycompact/dhall-haskell";
        ref = "master";
        rev = "f3be2bc388c8cee6ca1426537a7ee5a7073d2dc9";
      };

      techlab = builtins.fetchGit {
        url = "https://gitlab.com/homotopic-tech/techlab";
        ref = "master";
        rev = "88f77113eefb038ae33d1db353a8fbad704a67ad";
      };

      pandoc = builtins.fetchGit {
        url = "https://github.com/locallycompact/pandoc";
        ref = "master";
        rev = "d1349d2bbaeb679c46922eecb2df35a391df6cda";
      };
      # Additional ignore patterns to keep the Nix src clean
      ignorance = [
        "*.md"
        "figlet"
        "*.nix"
        "*.sh"
        "*.yml"
        "result"
      ];


      # Get some utilities
      inherit (import (shpadoinkle + "/nix/util.nix") { inherit compiler isJS pkgs; }) compilerjs gitignore doCannibalize;


      # Build faster by doing less
      chill = p: (pkgs.haskell.lib.overrideCabal p {
        inherit enableLibraryProfiling enableExecutableProfiling;
      }).overrideAttrs (_: {
        inherit doHoogle doHaddock strictDeps;
      });


      # Overlay containing Shpadoinkle packages, and needed alterations for those packages
      # as well as optimizations from Reflex Platform
      shpadoinkle-overlay =
        import (shpadoinkle + "/nix/overlay.nix") { inherit compiler chan isJS enableLibraryProfiling enableExecutableProfiling; };


      strip = x: pkgs.haskell.lib.dontHaddock (pkgs.haskell.lib.dontCheck x);
      # Haskell specific overlay (for you to extend)
      haskell-overlay = hself: hsuper: {
        "QuickCheck" = strip (hself.callHackage "QuickCheck" "2.14.2" { });
        "StateVar" = strip (hself.callHackage "StateVar" "1.2.2" { });
        "aeson-diff" = strip (hsuper.aeson-diff);
        "atomic-write" = strip (hself.callHackage "atomic-write" "0.2.0.7" { });
        "base-compat" = strip (hself.callHackage "base-compat" "0.11.2" { });
        "base64-bytestring" = strip hsuper.base64-bytestring;
        "blaze-builder" = strip (hself.callHackage "blaze-builder" "0.4.2.1" { });
        "blaze-html" = strip (hsuper.blaze-html);
        "case-insensitive" = strip (hself.callHackage "case-insensitive" "1.2.0.11" { });
        "cborg" = strip (hself.callHackage "cborg" "0.2.2.1" { });
        "cereal" = strip (hself.callHackage "cereal" "0.5.8.1" { });
        "chassis" = hself.callHackage "chassis" "0.0.5.0" { };
        "cmark-gfm" = strip hsuper.cmark-gfm;
        "conduit" = strip hsuper.conduit;
        "xml-conduit" = strip hsuper.xml-conduit;
        "citeproc" = strip (hself.callHackage "citeproc" "0.4.1" { });
        "commonmark" = strip (hself.callHackage "commonmark" "0.1.1.4" { });
        "commonmark-extensions" = strip (hself.callHackage "commonmark-extensions" "0.2.0.4" { });
        "commonmark-pandoc" = strip (hself.callHackage "commonmark-pandoc" "0.2.0.1" { });
        "comonad" = hself.callHackage "comonad" "5.0.8" { };
        "compdoc" = strip (hself.callHackage "compdoc" "0.3.0.0" { });
        "compdoc-dhall-decoder" = strip (hself.callHackage "compdoc-dhall-decoder" "0.3.0.0" { });
        "composite-aeson" = strip (hself.callHackage "composite-aeson" "0.7.5.0" { });
        "composite-aeson-throw" = strip (hself.callHackage "composite-aeson-throw" "0.1.0.0" { });
        "composite-base" = strip (hself.callHackage "composite-base" "0.7.5.0" { });
        "composite-dhall" = pkgs.haskell.lib.doJailbreak (hself.callHackage "composite-dhall" "0.0.1.0" { });
        "contravariant" = hself.callHackage "contravariant" "1.5.5" { };
        "data-fix" = strip (hself.callHackage "data-fix" "0.3.2" { });
        "dimensional" = strip (hself.callHackage "dimensional" "1.4" { });
        "dhall" = pkgs.haskell.lib.doJailbreak (strip (hself.callCabal2nix "dhall" (dhall + "/dhall") { }));
        "edit-distance" = strip hsuper.edit-distances;
        "edit-distance-vector" = strip hsuper.edit-distance-vector;
        "either" = strip (hself.callHackage "either" "5.0.1.1" { });
        "exact-pi" = strip hsuper.exact-pi;
        "exceptions" = strip (hsuper.exceptions);
        "first-class-families" = strip (hself.callHackage "first-class-families" "0.8.0.1" { });
        "foldl" = strip (hself.callHackage "foldl" "1.4.10" { });
        "formatting" = strip (hself.callHackage "formatting" "7.1.3" { });
        "half" = strip hsuper.half;
        "happy" = strip hsuper.happy;
        "hslua" = strip (hself.callHackage "hslua" "1.0.3.2" { });
        "hslua-module-path" = pkgs.haskell.lib.doJailbreak (strip (hself.callHackage "hslua-module-path" "0.1.0.1" { }));
        "hslua-module-system" = strip (hself.callHackage "hslua-module-system" "0.2.2.1" { });
        "hslua-module-text" = strip (hself.callHackage "hslua-module-text" "0.2.1" { });
        "hourglass" = strip hsuper.hourglass;
        "http-client" = strip hsuper.http-client;
        "ghcjs-dom-jsffi" = strip (hself.callHackage "ghcjs-dom-jsffi" "0.9.4.0" { });
        "ghcjs-xhr" = strip (hself.callHackage "ghcjs-xhr" "0.1.0.0" { });
        "indexed-traversable" = strip (hself.callHackage "indexed-traversable" "0.1.1" { });
        "indexed-traversable-instances" = strip (hself.callHackage "indexed-traversable-instances" "0.1" { });
        "integer-logarithms" = strip (hself.callHackage "integer-logarithms" "1.0.3.1" { });
        "memory" = strip (hself.callHackage "memory" "0.16.0" { });
        "microlens-aeson" = strip hsuper.microlens-aeson;
        "mono-traversable" = strip hsuper.mono-traversable;
        "network-uri" = strip hsuper.network-uri;
        "optics" = strip (hself.callHackage "optics" "0.3" { });
        "optics-core" = strip (hself.callHackage "optics-core" "0.3" { });
        "optics-extra" = strip (hself.callHackage "optics-extra" "0.3" { });
        "optics-th" = strip (hself.callHackage "optics-th" "0.3" { });
        "optparse-applicative" = strip (hsuper.optparse-applicative);
        "pandoc" = strip (hself.callCabal2nix "pandoc" pandoc { });
        "pandoc-throw" = strip (hself.callHackage "pandoc-throw" "0.1.0.0" { });
        "pandoc-types" = strip (hself.callHackage "pandoc-types" "1.22" { });
        "parsers" = strip (hself.callHackage "parsers" "0.12.10" { });
        "path" = strip (hself.callHackage "path" "0.9.0" { });
        "path-dhall-instance" = hself.callHackage "path-dhall-instance" "0.2.1.0" { };
        "pem" = strip (hsuper.pem);
        "polysemy" = strip (hself.callCabal2nix "polysemy" polysemy { });
        "polysemy-extra" = hself.callHackage "polysemy-extra" "0.2.0.0" { };
        "polysemy-fs" = hself.callHackage "polysemy-fs" "0.1.0.0" { };
        "polysemy-kvstore" = hself.callHackage "polysemy-kvstore" "0.1.2.0" { };
        "polysemy-methodology" = hself.callHackage "polysemy-methodology" "0.2.1.0" { };
        "polysemy-methodology-composite" = hself.callHackage "polysemy-methodology-composite" "0.1.4.0" { };
        "polysemy-path" = hself.callHackage "polysemy-path" "0.2.1.0" { };
        "polysemy-several" = hself.callHackage "polysemy-several" "0.1.0.0" { };
        "polysemy-vinyl" = hself.callHackage "polysemy-vinyl" "0.1.5.0" { };
        "prettyprinter-ansi-terminal" = strip (hself.callHackage "prettyprinter-ansi-terminal" "1.1.2" { });
        "prettyprinter" = strip (hself.callHackage "prettyprinter" "1.7.0" { });
        "pretty-simple" = strip (hsuper.pretty-simple);
        "repline" = pkgs.haskell.lib.doJailbreak (strip (hself.callHackage "repline" "0.3.0.0" { }));
        "rio" = strip (hsuper.rio);
        "serialise" = strip (hself.callHackage "serialise" "0.2.2.0" { });
        "splitmix" = strip (hself.callHackage "splitmix" "0.1.0.3" { });
        "strict" = strip (hself.callHackage "strict" "0.4.0.1" { });
        "streaming-commons" = strip (hself.callHackage "streaming-commons" "0.2.1.2" { });
        "tasty-bench" = hself.callHackage "tasty-bench" "0.3" { };
        "tagged" = hself.callHackage "tagged" "0.8.6.1" { };
        "techlab" = pkgs.haskell.lib.doJailbreak (hself.callCabal2nix "techlab" techlab { });
        "tf-random" = strip (hself.callHackage "tf-random" "0.5" { });
        "texmath" = strip (hself.callHackage "texmath" "0.12.3" { });
        "text-manipulate" = strip (hself.callCabal2nix "text-manipulate" text-manipulate { });
        "th-lift" = strip (hself.callHackage "th-lift" "0.8.2" { });
        "th-lift-instances" = strip (hself.callHackage "th-lift-instances" "0.1.18" { });
        "these" = strip (hself.callHackage "these" "1.1.1.1" { });
        "tls" = strip hsuper.tls;
        "time-compat" = strip (hself.callHackage "time-compat" "1.9.5" { });
        "transformers" = strip (hself.callHackage "transformers" "0.5.6.2" { });
        "type-errors" = strip hsuper.type-errors;
        "unliftio-path" = strip (hself.callHackage "unliftio-path" "0.0.2.0" { });
        "unicode-collation" = strip (hself.callHackage "unicode-collation" "0.1.3" { });
        "vinyl" = strip (hself.callHackage "vinyl" "0.13.0" { });
        "x509-validation" = strip hsuper.x509-validation;
        "x509" = strip hsuper.x509;
      };

      # Top level overlay (for you to extend)
      snowman-overlay = self: super: {
        haskell = super.haskell //
          {
            packages = super.haskell.packages //
              {
                ${compilerjs} = (super.haskell.packages.${compilerjs}.override
                  {
                    all-cabal-hashes = hackage;
                  }).override (old: {
                  overrides = super.lib.composeExtensions (old.overrides or (_: _: { })) haskell-overlay;
                });
              };
          };
      };


      # Complete package set with overlays applied
      pkgs = import
        (builtins.fetchTarball {
          url = "https://github.com/NixOS/nixpkgs/archive/${chan}.tar.gz";
        })
        {
          inherit system;
          overlays = [
            shpadoinkle-overlay
            snowman-overlay
          ];
        };


      ghcTools = with pkgs.haskell.packages.${compiler};
        [
          cabal-install
          ghcid
        ] ++ (if isJS then [ ] else [ stylish-haskell ]);


      # We can name him George
      snowman = pkgs.haskell.packages.${compilerjs}.callCabal2nix "snowman" (gitignore ignorance ../.) { };


    in
    with pkgs; with lib;

    {
      build =
        (if isJS && optimize then doCannibalize else x: x) (chill snowman);

      shell =
        pkgs.haskell.packages.${compilerjs}.shellFor {
          inherit withHoogle;
          packages = _: [ snowman ];
          COMPILER = compilerjs;
          buildInputs = ghcTools;
          shellHook = ''
            ${lolcat}/bin/lolcat ${../figlet}
            cat ${../intro}
          '';
        };
    }.${build-or-shell};
in
{
  build = f "build";
  shell = f "shell";
}
