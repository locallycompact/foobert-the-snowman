{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import qualified Control.Lens as L
import Data.Text
import qualified Dhall.Pretty
import Prettyprinter.Render.Text (renderStrict)
import Shpadoinkle (Html, JSM, NFData)
import Shpadoinkle.Backend.Snabbdom (runSnabbdom, stage)
import Shpadoinkle.Html
import Shpadoinkle.Run (live, runJSorWarp, simple)
import Techlab
import Techlab.Composite
import Techlab.Dhall as D

data T = T
  { foo :: Text,
    bar :: Text
  }
  deriving (Generic, NFData, Eq, Show, FromDhall, ToDhall)

withLensesAndProxies
  [d|
    type FId = "id" :-> Text

    type FT = "t" :-> T
    |]

encodeDhall :: ToDhall a => a -> Text
encodeDhall = renderStrict . layout . prettyCharacterSet Unicode . D.embed inject

view :: RElem FT xs => Record xs -> Html m (Record xs)
view x = div_ [text $ encodeDhall $ L.view fT x]

app :: JSM ()
app = simple runSnabbdom f view stage

dev :: IO ()
dev = live 8080 app

f :: Record '[FT, FId]
f = T "aa" "bb" :*: "ff" :*: RNil

main :: IO ()
main = do
  putStrLn $ "\nhi, my name is " <> show (L.view fId f)
  putStrLn "happy point of view on https://localhost:8080\n"
  runJSorWarp 8080 app
